<?php
declare(strict_types=1);

namespace App\Parser;

use App\Exception\InvalidParserException;

class XmlParser implements ParserInterface
{
    public static function getType(): string
    {
        return 'xml';
    }

    public function parse(string $content): array
    {
        return (array)simplexml_load_string($content);
    }

}