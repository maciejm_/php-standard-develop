<?php
declare(strict_types=1);

namespace App\Parser;

use App\Exception\InvalidParserException;

class JsonParser implements ParserInterface
{
    public static function getType(): string
    {
        return 'json';
    }

    public function parse(string $content): array
    {
        return json_decode($content, true);
    }
}