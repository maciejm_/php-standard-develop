<?php
declare(strict_types=1);

namespace App\Parser;


use App\Exception\InvalidParserException;

class Factory implements FactoryInterface
{
    protected iterable $parsers;

    public function __construct(iterable $parsers)
    {
        $this->parsers = $parsers;
    }

    public function getParser(string $type): ParserInterface
    {
        /** @var ParserInterface $parser */
        foreach ($this->parsers as $parser) {
            if ($parser::getType() === $type) {
                return $parser;
            }
        }

        throw new InvalidParserException(sprintf('Parser %s not found.', $type));
    }
}