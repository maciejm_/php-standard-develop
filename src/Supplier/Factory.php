<?php

namespace App\Supplier;

use App\Event\IntegrationEvents;
use App\Exception\SupplierNotFoundException;
use App\Listener\ProductsListener;
use App\Parser\FactoryInterface as ParserFactoryInterface;
use Psr\EventDispatcher\EventDispatcherInterface;

class Factory implements FactoryInterface
{
    const SUPPLIER_1 = 'supplier1';
    const SUPPLIER_2 = 'supplier2';
    const SUPPLIER_3 = 'supplier3';

    protected ParserFactoryInterface $parserFactory;

    protected EventDispatcherInterface $eventDispatcher;

    public function __construct(ParserFactoryInterface $parserFactory, EventDispatcherInterface $eventDispatcher)
    {
        $this->parserFactory = $parserFactory;
        $this->eventDispatcher = $eventDispatcher;

        $this->eventDispatcher->addListener(
            IntegrationEvents::SUPPLIER_GET_PRODUCTS,
            array(new ProductsListener(), 'logProducts')
        );
    }

    public function getSupplier(string $supplierName): SupplierInterface
    {
        $className = 'App\\Supplier\\' . ucfirst($supplierName);

        if (!class_exists($className)) {
            throw new SupplierNotFoundException(sprintf('Class %s not found.', $supplierName));
        }

        return new $className($this->parserFactory->getParser($className::getResponseType()), $this->eventDispatcher);
    }
}
