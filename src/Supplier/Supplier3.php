<?php

namespace App\Supplier;

class Supplier3 extends SupplierAbstract
{
    public static function getName(): string
    {
        return 'supplier3';
    }

    public static function getResponseType(): string
    {
        return 'json';
    }

    protected function parseResponse(): array
    {
        return $this->parser->parse($this->getResponse())['list'];
    }

    protected function getResponse(): string|bool
    {
        return file_get_contents('http://localhost/suppliers/supplier3.json');
    }
}
