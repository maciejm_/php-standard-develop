<?php

namespace App\Supplier;

class Supplier2 extends SupplierAbstract
{
    public static function getName(): string
    {
        return 'supplier2';
    }

    public static function getResponseType(): string
    {
        return 'xml';
    }

    protected function parseResponse(): array
    {
        $parsedResponse = $this->parser->parse($this->getResponse());

        return array_map(function ($element) {
            return (array)$element;
        }, $parsedResponse['item']);
    }

    protected function getResponse(): string|bool
    {
        return file_get_contents('http://localhost/suppliers/supplier2.xml');
    }
}
